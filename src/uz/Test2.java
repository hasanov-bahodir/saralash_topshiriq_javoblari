package uz;
// Bahodir Hasanov 7/3/2022 5:49 PM

import java.util.Scanner;

public class Test2 {
    public static void main(String[] args) {
        /**
         * Izoh: Bizga ma'lumki eng kichik tup son 2 ga teng,
         * umimiy ko'paytmadagi nollar soni tup sonlar ko'paytmasidan hosil bo'lgan 10 ga bog'liq,
         * buning uchun 10 ning tup bo'luvchilariga ko'payishi kerak
         * ya'ni 2 va 5 ga, bu sonlardan boshqa har qanday 2 va 5 raqamlari bilan tugagan sonlar murakkab,
         * demak agar bizga berilgan son 5 dan(no inclusive) kichik bo'lsa no'l bilan tugamaydi aks holda
         * faqat va faqat bitta no'l bilan tugaydi.
         */


        System.out.print("Sonni kiriting: ");
        int n = new Scanner(System.in).nextInt();
        System.out.println(numberOfZeros(n));

    }

    /**
     * Time Complexity O(1).
     */
    public static int numberOfZeros(int n) {
        if (n >= 5) return 1;
        return 0;
    }
}
