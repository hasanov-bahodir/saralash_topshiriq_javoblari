package uz;
// Bahodir Hasanov 7/3/2022 5:40 PM

public class Test1 {
    public static void main(String[] args) {
        int[][] arr =new int[7][7];
        arr[4][0] = 1;
        System.out.println(howManyStepsToTheCenterOfTheArray(arr));
    }
    public static int howManyStepsToTheCenterOfTheArray(int [][] arr){
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if(arr[i][j]==1){
                    return (Math.abs(3-i)+Math.abs(3-j));
                }
            }
        }
        return 0;
    }
}
